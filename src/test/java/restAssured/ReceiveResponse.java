package restAssured;

import Consist.EndPoints;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.testng.Assert;
import org.testng.annotations.Test;
import javax.xml.ws.Response;
import java.util.Random;
import static io.restassured.RestAssured.*;

public class ReceiveResponse {

    //How to call Post Method and Receive response in  Java Object in RestAssured
    @Test
    public void postMeth4(){

    int randomID = new Random().nextInt(1000);
    JasonInputData jasonInputData = new JasonInputData(randomID,"Title" + randomID, "author"+ randomID);
    JsonReceiveData jasonReceiveData = new JsonReceiveData();

    Gson gson = new GsonBuilder().create();   //convert data in to an object

        baseURI="http://localhost:3000/";
        String url = EndPoints.CHAPTERPOINT2;

        io.restassured.response.Response response =
                given()
                .header("Content-Type","application/json")
                .and()
                .body(jasonInputData)

                .when()
                .post(url)

                .then()
                .statusCode(201)
                .log()
                .all()
                .extract()
                .response();

        jasonReceiveData = gson.fromJson(response.prettyPrint(),JsonReceiveData.class);
        //response convert into JsonReceiveData.class

        Assert.assertEquals(jasonReceiveData.getId(),jasonInputData.getId());
        Assert.assertEquals(jasonReceiveData.getTitle(),jasonInputData.getTitle());
        Assert.assertEquals(jasonReceiveData.getAuthor(),jasonInputData.getAuthor());

    }

}

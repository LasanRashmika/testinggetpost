package restAssured;
import static io.restassured.RestAssured.*;
import Consist.EndPoints;
import com.google.gson.JsonObject;
import org.testng.annotations.Test;
import java.util.Random;

public class TestChapter2 {

    //How to call Post Method using Json Object in RestAssured
    @Test
    public void postMethod(){

      JsonObject json = new JsonObject();
        int randomID = new Random().nextInt(1000);

        json.addProperty("id", randomID);
        json.addProperty("title","myTitle"+randomID);
        json.addProperty("author","Arthur"+randomID);

        baseURI="http://localhost:3000/";
        String url = EndPoints.CHAPTERPOINT2;

        given()
                .header("Content-Type","application/json")
                .body(json.toString())

                .when()
                .post(url)

                .then()
                .statusCode(201)
                .log()
                .all();
    }
}

package restAssured;
import Consist.EndPoints;
import org.testng.annotations.Test;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

public class TestChapter3 {

//How to call Post Method using Json file in RestAssured
    @Test
    public void postMeth() throws FileNotFoundException {

        File xx = new File("E:\\LearningMe\\WeatherAPI\\jsonFileInput\\jsonrequest1.json");
        InputStream ff = new FileInputStream(xx);

        baseURI="http://localhost:3000/";
        String url = EndPoints.CHAPTERPOINT2;

        given()
                .header("Content-Type","application/json")
                .and()
                .body(ff)

                .when()
                .post(url)

                .then()
                .statusCode(201)
                .log()
                .all();

    }
}

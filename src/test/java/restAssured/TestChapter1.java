package restAssured;
import Consist.EndPoints;
import io.restassured.http.ContentType;
import org.testng.annotations.Test;

import javax.xml.ws.Endpoint;

import static io.restassured.RestAssured.*;


public class TestChapter1 {
    @Test
    public void getMethod(){
        baseURI="http://localhost:3000/";
        String url = EndPoints.CHAPENDPOINT1;

        given().contentType("application/json")
                .when().get(url)
                .then().statusCode(200).log().all();
    }

}
